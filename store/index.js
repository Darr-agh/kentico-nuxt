export const state = () => ({

  //Pages
  homepage: null,
  about: null
})

export const mutations = {
  setHomepage(state, content) {
    state.homepage = content
  },
  setAbout(state, content) {
    state.about = content
  }
}

// export const getters = {
//   getContent(state) {
//     return state.content
//   }
// }

export const actions = {

  getHome(context) {

    return this.$deliveryClient
      .items()
      .type('homepage')
      .depthParameter(2)
      .getPromise()
      .then(response => {
        context.commit('setHomepage', response.items[0]);
      });
  },

  getAbout(context) {

    return this.$deliveryClient
      .items()
      .type('about')
      .depthParameter(2)
      .getPromise()
      .then(response => {
        context.commit('setAbout', response.items[0]);
      });
  }
}

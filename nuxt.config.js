
module.exports = {
  server: {
    port: 3001, // default: 3000
  },
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'kenticocloud-nuxt-module'
  ],
  kenticocloud: {
    projectId: 'cef43aa0-5d18-00b3-7411-869b39d022b6',
    enableAdvancedLogging: false,
    previewApiKey:
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiOWMwYjQ1ZmQ4YjQ0YjhhYjk3ZjlhYmQzMTc0YTdhYiIsImlhdCI6IjE1NjAxOTI1MDEiLCJleHAiOiIxOTA1NzkyNTAxIiwicHJvamVjdF9pZCI6ImNlZjQzYWEwNWQxODAwYjM3NDExODY5YjM5ZDAyMmI2IiwidmVyIjoiMS4wLjAiLCJhdWQiOiJwcmV2aWV3LmRlbGl2ZXIua2VudGljb2Nsb3VkLmNvbSJ9.1AdMagFDs5o_Qoo4QtpHUf5SPbsgpubqmYw7uBkeQQU',
    enablePreviewMode: true
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
